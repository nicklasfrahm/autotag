package internal

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// AlignNodePackageFiles will ensure that both the "package.json" and the "package-lock.json"
// have the appropriate version number. The function returns a list of modified files as a
// string array to indicate, which files were modified.
func AlignNodePackageFiles(version string) []string {
	var modifiedFiles []string
	packageFiles := []string{"package.json", "package-lock.json"}

	// Open all package files.
	for _, packageFile := range packageFiles {
		// If the file exists, check the version number.
		if fileHandle, err := os.OpenFile(packageFile, os.O_RDWR, 0600); err == nil {
			// Defer closing the file to automatically close it.
			defer fileHandle.Close()

			// Read file.
			byteValue, err := ioutil.ReadAll(fileHandle)
			if err != nil {
				log.Fatalf("error: reading file failed: %s: %s", packageFile, err)
			}

			// Parse JSON.
			var jsonFile map[string]interface{}
			err = json.Unmarshal(byteValue, &jsonFile)
			if err != nil {
				log.Fatalf("error: parsing JSON file failed: %s: %s", packageFile, err)
			}

			// Print version number.
			if version != jsonFile["version"] {
				// Add file to modified files list.
				modifiedFiles = append(modifiedFiles, packageFile)

				// Override version.
				jsonFile["version"] = version

				// Create JSON file.
				modifiedByteValue, err := json.MarshalIndent(jsonFile, "", "  ")
				if err != nil {
					log.Fatalf("error: marshalling JSON failed: %s\n", err)
				}

				// Write modified JSON file.
				_, err = fileHandle.WriteAt(modifiedByteValue, 0)
				if err != nil {
					log.Fatalf("error: updating version failed: %s\n", err)
				}
			}
		}
	}

	return modifiedFiles
}
