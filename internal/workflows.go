package internal

// IsProtectedBranch will return a boolean to indicate whether the branch is a protected branch.
// Currently only the GitHub and Git flow are supported.
func IsProtectedBranch(branch string) bool {
	// Define the protected branches for the supported workflows.
	protectedBranches := map[string]bool{
		"master":  true,
		"release": true,
		"develop": true,
	}

	// Perform simple map lookup.
	return protectedBranches[branch]
}
