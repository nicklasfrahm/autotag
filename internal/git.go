package internal

import (
	"log"
	"os/exec"
	"strings"
)

// LatestGitTag returns the latest tag in the Git repository.
// If there is no tag in the repository it will return an empty string.
func LatestGitTag() string {
	// Execute command.
	out, err := exec.Command("git", "describe", "--abbrev=0", "--tags").Output()
	if err != nil {
		// Check if exit error is caused by missing tags.
		exitErr, _ := err.(*exec.ExitError)
		if string(exitErr.Stderr) == "fatal: No names found, cannot describe anything.\n" {
			return ""
		}
		log.Fatalf("error: reading repository tags failed: %s", err)
	}

	return strings.TrimSpace(string(out))
}

// GitConfigUser configures the Git user used for commits.
func GitConfigUser(gitUser string, gitEmail string) {
	// #nosec: Set Git commit user name.
	_, err := exec.Command("git", "config", "user.name", gitUser).Output()
	if err != nil {
		log.Fatalf("error: setting git user name failed: %s", err)
	}

	// #nosec: Set Git commit user email.
	_, err = exec.Command("git", "config", "user.email", gitEmail).Output()
	if err != nil {
		log.Fatalf("error: setting git user email failed: %s", err)
	}
}

// GitCommitModified creates a commit that includes all changes.
func GitCommitModified(commitMessage string) {
	// #nosec: Create a commit.
	_, err := exec.Command("git", "commit", "-am", commitMessage).Output()
	if err != nil {
		log.Fatalf("error: creating commit failed: %s", err)
	}
}

// GitTagAnnotated creates an annotated Git tag.
func GitTagAnnotated(tagName string, tagMessage string) {
	// #nosec: Create an annotated tag.
	_, err := exec.Command("git", "tag", "-a", tagName, "-m", tagMessage).Output()
	if err != nil {
		log.Fatalf("error: creating tag failed: %s", err)
	}
}

// GitRemoteSetURL configures the URL for a named Git remote.
func GitRemoteSetURL(remoteName string, remoteURL string) {
	// #nosec: Set the remote URL.
	_, err := exec.Command("git", "remote", "set-url", remoteName, remoteURL).Output()
	if err != nil {
		log.Fatalf("error: setting remote URL failed: %s", err)
	}
}

// GitPush pushes commits to the Git remote.
func GitPush() {
	// Push commits to remote.
	_, err := exec.Command("git", "push").Output()
	if err != nil {
		log.Fatalf("error: pushing commits to remote failed: %s", err)
	}
}

// GitPushTags pushes tags to the Git remote.
func GitPushTags() {
	// Push tags to remote.
	_, err := exec.Command("git", "push", "--tags").Output()
	if err != nil {
		log.Fatalf("error: pushing tags to remote failed: %s", err)
	}
}
