package internal

import (
	"log"
	"os"
)

// Config returns the configuration of the script based on the required environment variables.
// The required environment variables are:
// - AUTOTAG_GIT_NAME
// - AUTOTAG_GIT_EMAIL
// - AUTOTAG_HTTP_USERNAME
// - AUTOTAG_HTTP_PASSWORD
// - AUTOTAG_HELM_CHARTS_IGNORE
func Config() map[string]string {
	// Check if environment variables are avaialable.
	var incompleteConfig bool
	config := map[string]string{
		"AUTOTAG_GIT_NAME":           "",
		"AUTOTAG_GIT_EMAIL":          "",
		"AUTOTAG_HTTP_USERNAME":      "",
		"AUTOTAG_HTTP_PASSWORD":      "",
		"AUTOTAG_HELM_CHARTS_IGNORE": "",
	}
	optionalConfigVars := []string{"AUTOTAG_HELM_CHARTS_IGNORE"}

	// Get configuration from environment variables.
	for key := range config {
		config[key] = os.Getenv(key)
		if len(config[key]) == 0 {
			// Check if config variable is optional.
			var isOptional bool
			var index int
			for i, optionalConfigVar := range optionalConfigVars {
				if key == optionalConfigVar {
					isOptional = true
					index = i
					break
				}
			}

			// Display error for required environment variables.
			if !isOptional {
				// Print error without exiting program to look for further missing config keys.
				log.Printf("error: missing or empty environment variable: %s\n", key)
				incompleteConfig = true
			} else {
				// Remove optional config variable to increase speed for further lookups, if any.
				optionalConfigVars = append(optionalConfigVars[:index], optionalConfigVars[index+1:]...)
			}
		}
	}

	// Exit program if required environment variable was missing.
	if incompleteConfig {
		os.Exit(1)
	}

	return config
}
