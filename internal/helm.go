package internal

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

// AlignHelmChartFiles will ensure that the "Chart.yml" contains the appropriate
// "appVersion" number. The function returns a list of modified helm chart files
// as an array of strings.
func AlignHelmChartFiles(version string, ignoredHelmCharts []string) []string {
	var modifiedFiles []string

	files, err := ioutil.ReadDir("helm")
	// Ignore if helm directory does not exist.
	if err != nil {
		return modifiedFiles
	}

	for _, file := range files {
		// Check if file is a directory.
		if file.IsDir() {
			// Check if helm chart is ignored.
			var isIgnored bool
			var index int
			fileName := file.Name()
			for i, ignoredHelmChart := range ignoredHelmCharts {
				if fileName == ignoredHelmChart {
					isIgnored = true
					index = i
					break
				}
			}

			// Process found helm charts.
			if !isIgnored {
				// Assemble file path.
				filePath := "helm/" + fileName + "/Chart.yaml"

				// If the file exists, check the version number.
				if fileHandle, err := os.OpenFile(filePath, os.O_RDWR, 0600); err == nil {
					// Defer closing the file to automatically close it.
					defer fileHandle.Close()

					// Read file.
					byteValue, err := ioutil.ReadAll(fileHandle)
					if err != nil {
						log.Fatalf("error: reading file failed: %s: %s", filePath, err)
					}

					// Parse YAML.
					var yamlFile map[string]interface{}
					err = yaml.Unmarshal(byteValue, &yamlFile)
					if err != nil {
						log.Fatalf("error: parsing YAML file failed: %s: %s", filePath, err)
					}

					// Print version number.
					if version != yamlFile["appVersion"] {
						// Add file to modified files list.
						modifiedFiles = append(modifiedFiles, filePath)

						// Override version.
						yamlFile["appVersion"] = version

						// Create JSON file.
						modifiedByteValue, err := yaml.Marshal(yamlFile)
						if err != nil {
							log.Fatalf("error: marshalling YAML failed: %s\n", err)
						}

						// Write modified YAML file.
						_, err = fileHandle.WriteAt(modifiedByteValue, 0)
						if err != nil {
							log.Fatalf("error: updating appVersion failed: %s\n", err)
						}
					}
				}
			} else {
				// Remove ignored helm chart from list to increase speed for further lookups, if any.
				ignoredHelmCharts = append(ignoredHelmCharts[:index], ignoredHelmCharts[index+1:]...)
			}
		}
	}

	return modifiedFiles
}
