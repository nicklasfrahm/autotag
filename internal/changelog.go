package internal

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strings"
)

// LatestChangelogVersion returns the version of the top-most entry in the "CHANGELOG.md".
func LatestChangelogVersion() string {
	// File path of the changelog.
	changelogPath := "CHANGELOG.md"

	// Open changelog for parsing.
	file, err := os.Open(changelogPath)
	if err != nil {
		log.Fatalf("error: opening changelog failed: %s", err)
	}

	// Ensure that the file is closed, once the function returns.
	defer file.Close()

	// Create new scanner to process file line by line.
	scanner := bufio.NewScanner(file)
	// Regular expression for version numbers.
	versionRegex := regexp.MustCompile("^##\\s[^\n]+$")
	// Find the first line that matches the version number.
	for scanner.Scan() {
		line := scanner.Text()
		if versionRegex.MatchString(line) {
			versionChunks := strings.Split(line, " ")
			return strings.NewReplacer("[", "", "]", "").Replace(versionChunks[1])
		}
	}

	// Handle scanner errors.
	if err := scanner.Err(); err != nil {
		log.Fatalf("error: reading changelog failed: %s", err)
	}

	// Return empty string if no match was found.
	log.Fatalln("error: parsing changelog version failed: no tags found")
	return ""
}
