package main

import (
	"log"
	"net/url"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/nicklasfrahm/autotag/internal"
)

func main() {
	// Configure logging.
	log.SetFlags(log.LstdFlags | log.LUTC)

	// Check if "git" is available in path.
	if _, err := exec.LookPath("git"); err != nil {
		log.Fatalf("error: dependency not installed: \"git\": %s", err)
	}

	// Check branch name and append suffixes to version number if necessary.
	branch := os.Getenv("CI_COMMIT_REF_NAME")

	// Check if branch is protected.
	if internal.IsProtectedBranch(branch) {
		// Show that a protected branch is being used.
		log.Printf("Detected protected branch: %s\n", branch)
	} else {
		// Show that a feature branch is being used.
		log.Printf("Detected feature branch: %s\n", branch)
	}

	// Read the latest tag from the Git repo.
	latestGitTag := internal.LatestGitTag()
	if len(latestGitTag) == 0 {
		log.Println("Git version: No tags found.")
	} else {
		log.Printf("Git version: %s\n", latestGitTag)
	}

	// Parse changelog to find most recent documented version.
	changelogVersion := internal.LatestChangelogVersion()
	log.Printf("Changelog version: %s\n", changelogVersion)

	// Check case-insensitive if version is "unreleased".
	if strings.EqualFold(changelogVersion, "unreleased") {
		log.Println("Version is unreleased.")
		log.Println("Aborting creation of tag.")
		os.Exit(0)
	}

	// Check if the changelog version is equal to the Git version.
	if len(latestGitTag) != 0 && changelogVersion == latestGitTag {
		log.Println("Version already exists.")
		log.Println("Aborting creation of tag.")
		os.Exit(1)
	}

	// Check if branch is protected.
	if internal.IsProtectedBranch(branch) {
		// Get configuration from environment variables.
		config := internal.Config()

		// Align version numbers for node package files.
		nodePackageFilesModified := internal.AlignNodePackageFiles(changelogVersion)
		if len(nodePackageFilesModified) != 0 {
			for _, modifiedFile := range nodePackageFilesModified {
				log.Printf("Updating version number: %s\n", modifiedFile)
			}
		}

		// Align version numbers for helm chart files.
		ignoredHelmCharts := strings.Split(config["AUTOTAG_HELM_CHARTS_IGNORE"], ",")
		helmChartFilesModified := internal.AlignHelmChartFiles(changelogVersion, ignoredHelmCharts)
		if len(helmChartFilesModified) != 0 {
			for _, modifiedFile := range helmChartFilesModified {
				log.Printf("Updating version number: %s\n", modifiedFile)
			}
		}

		// Configure Git user.
		log.Println("Setting up git user ...")
		internal.GitConfigUser(config["AUTOTAG_GIT_NAME"], config["AUTOTAG_GIT_EMAIL"])

		// Configure credentials for Git remote URL.
		remoteURL, err := url.Parse(os.Getenv("CI_REPOSITORY_URL"))
		if err != nil {
			log.Fatalf("error: parsing remote URL failed: %s", err)
		}
		remoteURL.User = url.UserPassword(config["AUTOTAG_HTTP_USERNAME"], config["AUTOTAG_HTTP_PASSWORD"])
		log.Println("Updating git remote ...")
		internal.GitRemoteSetURL("origin", remoteURL.String())

		// Check if any files were modified due to version alignment.
		if len(nodePackageFilesModified) == 0 && len(helmChartFilesModified) == 0 {
			// Create annotated tag.
			log.Println("Creating git tag ...")
			internal.GitTagAnnotated(changelogVersion, "For changes, refer to the CHANGELOG.md.")

			// Push tags to remote.
			log.Println("Pushing git tag ...")
			internal.GitPushTags()
		} else {
			// Create commit.
			log.Println("Creating git commit ...")
			internal.GitCommitModified("Align version numbers")

			// Push commits to remote.
			log.Println("Pushing git commit ...")
			internal.GitPush()
		}
	}
}
