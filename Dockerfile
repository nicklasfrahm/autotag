# Define base image for build stage.
FROM golang AS build

# Define current working directory.
WORKDIR /app

# Copy source files.
COPY . ./

# Build binary.
RUN make

# Define base image for final Docker image.
FROM alpine

# Install git.
RUN apk add --no-cache git git-lfs

# Copy binary from build stage.
COPY --from=build /app/autotag /usr/bin/autotag
