# Define docker image architecture.
ARG ARCH

# Define base image for build stage.
FROM balenalib/${ARCH}-alpine-golang:latest-build AS build

# Start docker image cross build.
RUN [ "cross-build-start" ]

# Define current working directory.
WORKDIR /app

# Copy source files.
COPY . ./

# Build binary.
RUN make

# Revert back for normal execution.
RUN [ "cross-build-end" ]

# Define base image for final Docker image.
FROM balenalib/${ARCH}-alpine:latest-run AS run

# Start docker image cross build.
RUN [ "cross-build-start" ]

# Install git.
RUN apk add --no-cache git git-lfs && rm -rf /var/cache/apk/*

# Copy binary from build stage.
COPY --from=build /app/autotag /usr/bin/autotag

# Revert back for normal execution.
RUN [ "cross-build-end" ]
