# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Multi-arch docker images

## [1.0.0] - 2020-03-18

### Fixed

- Add `git-lfs` to Docker image

### Security

- Remove `stderr` to prevent leaking credentials

## [0.1.9] - 2020-03-18 [YANKED]

### Fixed

- Display `stderr` on failed commands

## [0.1.8] - 2020-03-18

### Added

- More output on failed commands

## [0.1.7] - 2020-02-23

### Fixed

- Run **compile** step of CI pipeline in daily builds and for tags
- Capitalize keywords in Dockerfile

## [0.1.6] - 2020-02-20

### Added

- Improve usage information in [README.md](README.md)
- Separate daily builds from builds for git tags

## [0.1.5] - 2020-02-20

### Added

- License scanning

### Security

- Fix SAST security risks
- Configure dependency scanning

## [0.1.4] - 2020-02-20

### Added

- [Go Report](https://goreportcard.com/) badge

### Fixed

- Spelling mistakes in comments

## [0.1.3] - 2020-02-20

### Added

- Configure static application security testing (SAST)

## [0.1.2] - 2020-02-19

### Changed

- Configure daily builds via CI pipeline to keep docker image up-to-date

## [0.1.1] - 2020-02-19

### Added

- More logging

### Fixed

- Use `--password-stdin` to supply Docker registry password in CI pipeline

## [0.1.0] - 2020-02-19

### Added

- Automatic alignment of version numbers in `package.json` and `package-lock.json`
- Automatic alignment of version numbers in `Chart.yaml`
- Ignoring of helm charts via `AUTOTAG_HELM_CHARTS_IGNORE` environment variable
