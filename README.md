# Autotag

[![Go Report](https://goreportcard.com/badge/gitlab.com/nicklasfrahm/autotag)](https://goreportcard.com/badge/gitlab.com/nicklasfrahm/autotag)

A small `golang` program to automatically create Git tags based on a `CHANGELOG.md` file.

Currently the following CI providers are supported:

- [GitLab CI][gitlab ci]

Currently the following toolchains are supported:

- generic
- [node][node]
- [helm][helm]

Currently the following Git flows are supported:

- Git flow
- GitHub flow

## Usage

Make sure you have [Git][git] installed as this app does not package it.

```bash
$ export AUTOTAG_GIT_NAME="Nicklas Frahm"
$ export AUTOTAG_GIT_EMAIL="nicklas.frahm@gmail.com"
$ export AUTOTAG_HTTP_USERNAME="nicklasfrahm"
$ export AUTOTAG_HTTP_PASSWORD="my-api-key"
$ ./autotag
```

The intended use case for this application is in GitLab's CI/CD pipeline. An example can be seen in the [`.gitlab-ci.yml`](.gitlab-ci.yml) as this application uses itself for automatic tagging.

## Workflow

This section describes the basic algorithm `autotag` will execute.

1. Verify dependencies.

   - Check if [Git][git] is installed.

1. Read latest tag.

1. Parse changelog to find current version.

1. Abort if version is `Unreleased`.

1. Fail if version in changelog is equal to the latest tag.

1. If current branch is `master`, `release` or `develop`.

   1. Verify if required information is provided via environment variables.

      - `AUTOTAG_GIT_NAME`
      - `AUTOTAG_GIT_EMAIL`
      - `AUTOTAG_HTTP_USERNAME`
      - `AUTOTAG_HTTP_PASSWORD`

   1. Detect language stack and align version numbers.

      - If a `node` toolchain is recognized, make sure that the `package.json` and `package-lock.json` have the same version.
      - If a `helm` toolchain is recognized, make sure that the `appVersion` is up-to-date.

   1. If files were modified due to version number alignment, create a commit to align version numbers.

   1. If no files were modified, tag commit with version number from changelog.

   1. Add remote and push tag or commits.

[gitlab ci]: https://about.gitlab.com/product/continuous-integration/
[git]: https://git-scm.com
[node]: https://nodejs.org/en/
[helm]: https://helm.sh
