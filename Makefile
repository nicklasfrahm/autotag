GROUP_NAME=nicklasfrahm
MODULE_NAME=autotag
GIT_NAME=test
GIT_EMAIL=test@example.com
HTTP_USERNAME=test
HTTP_PASSWORD=test
HELM_CHARTS_IGNORE=b,c
REGISTRY=registry.gitlab.com
REMOTE=$(shell git remote get-url origin)
COMMIT_REF_NAME=$(shell git rev-parse --abbrev-ref HEAD)
TAG=dev

.PHONY: build
build:
	go build -v -o $(MODULE_NAME) main.go

.PHONY: run
run:
	env AUTOTAG_GIT_NAME="$(GIT_NAME)" AUTOTAG_GIT_EMAIL="$(GIT_EMAIL)" AUTOTAG_HTTP_USERNAME="$(HTTP_USERNAME)" AUTOTAG_HTTP_PASSWORD="$(HTTP_PASSWORD)" CI_REPOSITORY_URL="$(REMOTE)" CI_COMMIT_REF_NAME="$(COMMIT_REF_NAME)" go run main.go

.PHONY: docker
docker:
	docker build -t $(REGISTRY)/$(GROUP_NAME)/$(MODULE_NAME):$(TAG) .

.PHONY: publish
publish:
	docker push $(REGISTRY)/$(GROUP_NAME)/$(MODULE_NAME):$(TAG)
